﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
using GeneticAlgorithm;
using System.Threading;
using AwokeKnowing.GnuplotCSharp;

namespace AGMinFunction
{
    public partial class Form1 : Form
    {

        int generation;

        GraphPane curvesPane;

        LineItem bestFitnessCurve;

        LineItem worstFitnessCurve;

        LineItem averageFitnessCurve;

        PointPairList bestFitnessList = new PointPairList();
        PointPairList worstFitnessList = new PointPairList();
        PointPairList averageFitnessList = new PointPairList();

        Thread thread;
        AG ag;
        public Form1()
        {
            InitializeComponent();

            ag = new AG();

            generation = 0;
        }


        double function(double x)
        {
            double y = 0.0;

            y = Math.Abs(x * Math.Sin(Math.Sqrt(Math.Abs(x))));

            return y;
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            
                if (roulleteRadioButton.Checked == true)
                    ag.generateNewPopulationUsingRoullete();
                else
                    ag.generateNewPopulationUsingTourney();

                //double[] X = ag.getXValues();
                //double[] Y = ag.getYValues();
                //double[] Z = ag.getPopulationFitness();

                //GnuPlot.HoldOn();
                //GnuPlot.SPlot("abs(x*y*sin(y*(pi/4)))", "with pm3d");
                ////Thread.Sleep(2000);
                //GnuPlot.SPlot(X, Y, Z, "with points pointtype 7 lc rgb 'blue'");
                //GnuPlot.HoldOff();

                generation++;
                List<int> bestIndividual = new List<int>(ag.getBestIndividual());

                generationLabel.Text = generation.ToString();
                bestFitnessLabel.Text = ag.getBestIndividualFitness().ToString();
                worstFitnessLabel.Text = ag.getWorstIndividualFitness().ToString();
                meanFitness.Text = ag.getAverageFitness().ToString();

                drawCurves();
            
        }

        void nextGenerationThread()
        {
            while (true)
            {
                if (roulleteRadioButton.Checked == true)
                    ag.generateNewPopulationUsingRoullete();
                else
                    ag.generateNewPopulationUsingTourney();

                /*double[] X = ag.getXValues();
                double[] Y = ag.getYValues();
                double[] Z = ag.getPopulationFitness();

                GnuPlot.HoldOn();
                GnuPlot.SPlot("abs(x*y*sin(y*(pi/4)))", "with pm3d");
                //Thread.Sleep(2000);
                GnuPlot.SPlot(X, Y, Z, "with points pointtype 7 lc rgb 'blue'");
                GnuPlot.HoldOff();*/

                this.Invoke(new EventHandler(timer1_Tick));

                generation++;

                //generation++;

                //generationLabel.Text = generation.ToString();

                //string str = ag.checkIfIndividualIsValid(ag.getBestIndividual()) == true ? " - Valid" : " - Invalid";

                //ag.fitness(ag.getBestIndividual());

                //bestFitnessLabel.Text = ag.getBestIndividualFitness().ToString() + str;

                //worstFitnessLabel.Text = ag.getWorstIndividualFitness().ToString();
                //meanFitness.Text = ag.getAverageFitness().ToString();

                //printBetterIndiv();

                //drawCurves();
            }
        }

        void initCurves()
        {
            bestFitnessList = new PointPairList();
            worstFitnessList = new PointPairList();
            averageFitnessList = new PointPairList();

            curvesPane = zedGraphControl2.GraphPane;

            // Set the titles and axis labels
            curvesPane.Title.Text = "Fitness curves";
            curvesPane.XAxis.Title.Text = "Generation";
            curvesPane.YAxis.Title.Text = "Fitness";

            // Generate a red curve with diamond
            // symbols, and "Porsche" in the legend
            bestFitnessCurve = curvesPane.AddCurve("Best",
               bestFitnessList, Color.Green, SymbolType.None);

            worstFitnessCurve = curvesPane.AddCurve("Worst",
               worstFitnessList, Color.Red, SymbolType.None);

            averageFitnessCurve = curvesPane.AddCurve("Average",
               averageFitnessList, Color.Black, SymbolType.None);

            
        }

        void drawCurves()
        {
            
            bestFitnessList.Add(generation, ag.getBestIndividualFitness());
            worstFitnessList.Add(generation, ag.getWorstIndividualFitness());
            averageFitnessList.Add(generation, ag.getAverageFitness());

            // Calculate the Axis Scale Ranges
            zedGraphControl2.AxisChange();
            zedGraphControl2.Invalidate();
        }

        void clearCurves()
        {
            bestFitnessList.Clear();
            worstFitnessList.Clear();
            averageFitnessList.Clear();

            curvesPane.CurveList.Clear();

            // Calculate the Axis Scale Ranges
            zedGraphControl2.AxisChange();
            zedGraphControl2.Invalidate();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            generation = 0;

            //roulleteRadioButton.Checked = true;
            tourneyRadioButton.Checked = true;
            //drawFuntion();

            initCurves();
        }

        void drawInitialPop()
        {
            double crossOverRate = Convert.ToDouble(crossOverRateTextBox.Text);
            double mutateRate = Convert.ToDouble(mutateRateTextBox.Text);
            bool elitism = elitismCheckBox.Checked;

            int numberOfIndividuals = Convert.ToInt32(nbrIndvNumericUpDown.Value);
            
            ag.CrossOverRate = crossOverRate;
            ag.MutateRate = mutateRate;
            ag.Elitism = elitism;
            ag.NumIndvTourney = Convert.ToInt32(indvTourneyNumericUpDown.Value);
            ag.Heuristic = heuristicCheckBox.Checked;

            Point start = new Point(Convert.ToInt32(xPointNumericUpDown.Value), Convert.ToInt32(yPointNumericUpDown.Value));
            ag.InitialPoint = start;
            

            ag.InitialPopulation(numberOfIndividuals);

            ag.NumberOfBestIndividuals = Convert.ToInt32(nbrBestIndivNumericUpDown.Value);

            /*
            double[] X = ag.getXValues();
            double[] Y = ag.getYValues();
            double[] Z = ag.getPopulationFitness();

            double zmin = ag.function(xmin, ymin);
            double zmax = ag.function(xmax, ymax);

            GnuPlot.Set("isosamples 100");
            GnuPlot.Set("xrange[" + xmin.ToString() + ":" + xmax.ToString() + "]",
                        "yrange[" + ymin.ToString() + ":" + ymax.ToString() + "]",
                        "zrange[0:3000]");

            GnuPlot.HoldOn();
            GnuPlot.SPlot("abs(x*y*sin(y*(pi/4)))", "with pm3d");
            GnuPlot.SPlot(X, Y, Z, "with points pointtype 7 lc rgb 'blue'");
            GnuPlot.HoldOff();*/
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (button2.Enabled == true)
            {
                
                drawInitialPop();

                button1.Enabled = true;
                button2.Enabled = false;
                button3.Enabled = true;

                nbrIndvNumericUpDown.Enabled = false;
                

                crossOverRateTextBox.Enabled = false;
                mutateRateTextBox.Enabled = false;

                elitismCheckBox.Enabled = false;

                checkBox1.Enabled = true;

                List<int> bestIndividual = new List<int>(ag.getBestIndividual());

                generationLabel.Text = generation.ToString();
                bestFitnessLabel.Text = ag.getBestIndividualFitness().ToString();
                worstFitnessLabel.Text = ag.getWorstIndividualFitness().ToString();
                meanFitness.Text = ag.getAverageFitness().ToString();

                drawCurves();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            timer1.Stop();
                        
            
            ag = new AG();

            //drawFuntion();

            //zedGraphControl1.AxisChange();
            //zedGraphControl1.Invalidate();

            button2.Enabled = true;

            nbrIndvNumericUpDown.Enabled = true;
            

            crossOverRateTextBox.Enabled = true;
            mutateRateTextBox.Enabled = true;

            elitismCheckBox.Enabled = true;

            button1.Enabled = false;

            generationLabel.Text = "0";
            bestFitnessLabel.Text = "0";
            worstFitnessLabel.Text = "0";
            meanFitness.Text = "0";

            generation = 0;

            button3.Enabled = false;

            checkBox1.Enabled = false;

            if (checkBox1.Checked == true)
                checkBox1.Checked = false;

            clearCurves();

            initCurves();

            richTextBox1.Clear();
        }
        
        private void xminTextBox_Validated(object sender, EventArgs e)
        {
            //myPane.CurveList.Clear();
            //drawFuntion();
        }

        private void xmaxTextBox_Validated(object sender, EventArgs e)
        {
            //myPane.CurveList.Clear();
            //drawFuntion();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                thread = new Thread(nextGenerationThread);
                thread.Start();
            }
            else
            {
                if (thread != null)
                {
                    thread.Abort();
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {         
            List<int> bestIndividual = new List<int>(ag.getBestIndividual());

            generationLabel.Text = generation.ToString();

            ag.fitness(bestIndividual);
            ag.printPositions(bestIndividual);
            bestFitnessLabel.Text = ag.getBestIndividualFitness().ToString();
            worstFitnessLabel.Text = ag.getWorstIndividualFitness().ToString();
            meanFitness.Text = ag.getAverageFitness().ToString();

            drawCurves();
        }

        private void mutateRateTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        
        
    }
}
