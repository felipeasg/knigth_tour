﻿namespace AGMinFunction
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.crossOverRateTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.mutateRateTextBox = new System.Windows.Forms.TextBox();
            this.elitismCheckBox = new System.Windows.Forms.CheckBox();
            this.zedGraphControl2 = new ZedGraph.ZedGraphControl();
            this.nbrIndvNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.generationLabel = new System.Windows.Forms.Label();
            this.bestFitnessLabel = new System.Windows.Forms.Label();
            this.worstFitnessLabel = new System.Windows.Forms.Label();
            this.meanFitness = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.roulleteRadioButton = new System.Windows.Forms.RadioButton();
            this.tourneyRadioButton = new System.Windows.Forms.RadioButton();
            this.indvTourneyNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.nbrBestIndivNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.heuristicCheckBox = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.xPointNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.yPointNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nbrIndvNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.indvTourneyNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbrBestIndivNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xPointNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yPointNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(116, 213);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(139, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Next generation";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // crossOverRateTextBox
            // 
            this.crossOverRateTextBox.Location = new System.Drawing.Point(116, 112);
            this.crossOverRateTextBox.Name = "crossOverRateTextBox";
            this.crossOverRateTextBox.Size = new System.Drawing.Size(139, 20);
            this.crossOverRateTextBox.TabIndex = 2;
            this.crossOverRateTextBox.Text = "0.8";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Cross over rate:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Mutate rate:";
            // 
            // mutateRateTextBox
            // 
            this.mutateRateTextBox.Location = new System.Drawing.Point(116, 138);
            this.mutateRateTextBox.Name = "mutateRateTextBox";
            this.mutateRateTextBox.Size = new System.Drawing.Size(139, 20);
            this.mutateRateTextBox.TabIndex = 4;
            this.mutateRateTextBox.Text = "0.01";
            this.mutateRateTextBox.TextChanged += new System.EventHandler(this.mutateRateTextBox_TextChanged);
            // 
            // elitismCheckBox
            // 
            this.elitismCheckBox.AutoSize = true;
            this.elitismCheckBox.Checked = true;
            this.elitismCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.elitismCheckBox.Location = new System.Drawing.Point(116, 164);
            this.elitismCheckBox.Name = "elitismCheckBox";
            this.elitismCheckBox.Size = new System.Drawing.Size(55, 17);
            this.elitismCheckBox.TabIndex = 6;
            this.elitismCheckBox.Text = "Elitims";
            this.elitismCheckBox.UseVisualStyleBackColor = true;
            // 
            // zedGraphControl2
            // 
            this.zedGraphControl2.Location = new System.Drawing.Point(289, 37);
            this.zedGraphControl2.Name = "zedGraphControl2";
            this.zedGraphControl2.ScrollGrace = 0D;
            this.zedGraphControl2.ScrollMaxX = 0D;
            this.zedGraphControl2.ScrollMaxY = 0D;
            this.zedGraphControl2.ScrollMaxY2 = 0D;
            this.zedGraphControl2.ScrollMinX = 0D;
            this.zedGraphControl2.ScrollMinY = 0D;
            this.zedGraphControl2.ScrollMinY2 = 0D;
            this.zedGraphControl2.Size = new System.Drawing.Size(701, 353);
            this.zedGraphControl2.TabIndex = 7;
            // 
            // nbrIndvNumericUpDown
            // 
            this.nbrIndvNumericUpDown.Location = new System.Drawing.Point(117, 35);
            this.nbrIndvNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nbrIndvNumericUpDown.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nbrIndvNumericUpDown.Name = "nbrIndvNumericUpDown";
            this.nbrIndvNumericUpDown.Size = new System.Drawing.Size(138, 20);
            this.nbrIndvNumericUpDown.TabIndex = 12;
            this.nbrIndvNumericUpDown.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(0, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Number of individuals:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(116, 187);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(138, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "Generate initial population";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(116, 242);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(139, 23);
            this.button3.TabIndex = 18;
            this.button3.Text = "Reset";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(49, 348);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Generation:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(47, 373);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Best fitness:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(40, 396);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Worst fitness:";
            // 
            // generationLabel
            // 
            this.generationLabel.AutoSize = true;
            this.generationLabel.Location = new System.Drawing.Point(117, 348);
            this.generationLabel.Name = "generationLabel";
            this.generationLabel.Size = new System.Drawing.Size(13, 13);
            this.generationLabel.TabIndex = 22;
            this.generationLabel.Text = "0";
            // 
            // bestFitnessLabel
            // 
            this.bestFitnessLabel.AutoSize = true;
            this.bestFitnessLabel.Location = new System.Drawing.Point(117, 373);
            this.bestFitnessLabel.Name = "bestFitnessLabel";
            this.bestFitnessLabel.Size = new System.Drawing.Size(13, 13);
            this.bestFitnessLabel.TabIndex = 23;
            this.bestFitnessLabel.Text = "0";
            // 
            // worstFitnessLabel
            // 
            this.worstFitnessLabel.AutoSize = true;
            this.worstFitnessLabel.Location = new System.Drawing.Point(117, 396);
            this.worstFitnessLabel.Name = "worstFitnessLabel";
            this.worstFitnessLabel.Size = new System.Drawing.Size(13, 13);
            this.worstFitnessLabel.TabIndex = 24;
            this.worstFitnessLabel.Text = "0";
            // 
            // meanFitness
            // 
            this.meanFitness.AutoSize = true;
            this.meanFitness.Location = new System.Drawing.Point(117, 422);
            this.meanFitness.Name = "meanFitness";
            this.meanFitness.Size = new System.Drawing.Size(13, 13);
            this.meanFitness.TabIndex = 26;
            this.meanFitness.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(40, 422);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Mean fitness:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(51, 217);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(52, 17);
            this.checkBox1.TabIndex = 27;
            this.checkBox1.Text = "Timer";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // timer1
            // 
            this.timer1.Interval = 90;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // roulleteRadioButton
            // 
            this.roulleteRadioButton.AutoSize = true;
            this.roulleteRadioButton.Location = new System.Drawing.Point(116, 66);
            this.roulleteRadioButton.Name = "roulleteRadioButton";
            this.roulleteRadioButton.Size = new System.Drawing.Size(64, 17);
            this.roulleteRadioButton.TabIndex = 32;
            this.roulleteRadioButton.TabStop = true;
            this.roulleteRadioButton.Text = "Roullete";
            this.roulleteRadioButton.UseVisualStyleBackColor = true;
            // 
            // tourneyRadioButton
            // 
            this.tourneyRadioButton.AutoSize = true;
            this.tourneyRadioButton.Location = new System.Drawing.Point(116, 89);
            this.tourneyRadioButton.Name = "tourneyRadioButton";
            this.tourneyRadioButton.Size = new System.Drawing.Size(64, 17);
            this.tourneyRadioButton.TabIndex = 33;
            this.tourneyRadioButton.TabStop = true;
            this.tourneyRadioButton.Text = "Tourney";
            this.tourneyRadioButton.UseVisualStyleBackColor = true;
            // 
            // indvTourneyNumericUpDown
            // 
            this.indvTourneyNumericUpDown.Location = new System.Drawing.Point(186, 87);
            this.indvTourneyNumericUpDown.Name = "indvTourneyNumericUpDown";
            this.indvTourneyNumericUpDown.Size = new System.Drawing.Size(68, 20);
            this.indvTourneyNumericUpDown.TabIndex = 34;
            this.indvTourneyNumericUpDown.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(289, 396);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(701, 132);
            this.richTextBox1.TabIndex = 35;
            this.richTextBox1.Text = "";
            // 
            // nbrBestIndivNumericUpDown
            // 
            this.nbrBestIndivNumericUpDown.Location = new System.Drawing.Point(186, 161);
            this.nbrBestIndivNumericUpDown.Name = "nbrBestIndivNumericUpDown";
            this.nbrBestIndivNumericUpDown.Size = new System.Drawing.Size(68, 20);
            this.nbrBestIndivNumericUpDown.TabIndex = 36;
            this.nbrBestIndivNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // heuristicCheckBox
            // 
            this.heuristicCheckBox.AutoSize = true;
            this.heuristicCheckBox.Checked = true;
            this.heuristicCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.heuristicCheckBox.Location = new System.Drawing.Point(120, 282);
            this.heuristicCheckBox.Name = "heuristicCheckBox";
            this.heuristicCheckBox.Size = new System.Drawing.Size(67, 17);
            this.heuristicCheckBox.TabIndex = 37;
            this.heuristicCheckBox.Text = "Heuristic";
            this.heuristicCheckBox.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 319);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "Initial point:";
            // 
            // xPointNumericUpDown
            // 
            this.xPointNumericUpDown.Location = new System.Drawing.Point(115, 317);
            this.xPointNumericUpDown.Name = "xPointNumericUpDown";
            this.xPointNumericUpDown.Size = new System.Drawing.Size(68, 20);
            this.xPointNumericUpDown.TabIndex = 39;
            // 
            // yPointNumericUpDown
            // 
            this.yPointNumericUpDown.Location = new System.Drawing.Point(186, 317);
            this.yPointNumericUpDown.Name = "yPointNumericUpDown";
            this.yPointNumericUpDown.Size = new System.Drawing.Size(68, 20);
            this.yPointNumericUpDown.TabIndex = 40;
            this.yPointNumericUpDown.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(142, 302);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "X";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(213, 302);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 42;
            this.label6.Text = "Y";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 533);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.yPointNumericUpDown);
            this.Controls.Add(this.xPointNumericUpDown);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.heuristicCheckBox);
            this.Controls.Add(this.nbrBestIndivNumericUpDown);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.indvTourneyNumericUpDown);
            this.Controls.Add(this.tourneyRadioButton);
            this.Controls.Add(this.roulleteRadioButton);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.meanFitness);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.worstFitnessLabel);
            this.Controls.Add(this.bestFitnessLabel);
            this.Controls.Add(this.generationLabel);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.nbrIndvNumericUpDown);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.zedGraphControl2);
            this.Controls.Add(this.elitismCheckBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.mutateRateTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.crossOverRateTextBox);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "50";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nbrIndvNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.indvTourneyNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbrBestIndivNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xPointNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yPointNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox crossOverRateTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox mutateRateTextBox;
        private System.Windows.Forms.CheckBox elitismCheckBox;
        private ZedGraph.ZedGraphControl zedGraphControl2;
        private System.Windows.Forms.NumericUpDown nbrIndvNumericUpDown;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label generationLabel;
        private System.Windows.Forms.Label bestFitnessLabel;
        private System.Windows.Forms.Label worstFitnessLabel;
        private System.Windows.Forms.Label meanFitness;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.RadioButton roulleteRadioButton;
        private System.Windows.Forms.RadioButton tourneyRadioButton;
        private System.Windows.Forms.NumericUpDown indvTourneyNumericUpDown;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.NumericUpDown nbrBestIndivNumericUpDown;
        private System.Windows.Forms.CheckBox heuristicCheckBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown xPointNumericUpDown;
        private System.Windows.Forms.NumericUpDown yPointNumericUpDown;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

