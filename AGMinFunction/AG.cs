﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Security.Cryptography;

//http://interactivepython.org/runestone/static/pythonds/Graphs/graphdfs.html

namespace GeneticAlgorithm
{
    class AG
    {
        List<List<int>> population;
        List<List<int>> initialPopulation;
        int nbrIndv;
        int nbrGenes;
        double xmin;
        double xmax;
        
        double resolution;
        double crossOverRate;
        double mutateRate;
        int numIndvTourney;
        int numberOfBestIndividuals;

        Point initialPoint;

        public Point InitialPoint
        {
            get { return initialPoint; }
            set { initialPoint = value; }
        }
        
                
        Random randNum;

        bool elitism;

        bool maximizePl1;

        bool maximizePl2;

        bool maximizePl3;

        bool maximizePl4;

        bool heuristic;

        public bool Heuristic
        {
            get { return heuristic; }
            set { heuristic = value; }
        }
        public bool MaximizePl4
        {
            get { return maximizePl4; }
            set { maximizePl4 = value; }
        }

        public int NumberOfBestIndividuals
        {
            get { return numberOfBestIndividuals; }
            set { numberOfBestIndividuals = value; }
        }

        public bool MaximizePl3
        {
            get { return maximizePl3; }
            set { maximizePl3 = value; }
        }

        public bool MaximizePl2
        {
            get { return maximizePl2; }
            set { maximizePl2 = value; }
        }

        public bool MaximizePl1
        {
            get { return maximizePl1; }
            set { maximizePl1 = value; }
        }
        public int NumIndvTourney
        {
            get { return numIndvTourney; }
            set { numIndvTourney = value; }
        }
                

        public bool Elitism
        {
            get { return elitism; }
            set { elitism = value; }
        }

        public double MutateRate
        {
            get { return mutateRate; }
            set { mutateRate = value; }
        }

        public double CrossOverRate
        {
            get { return crossOverRate; }
            set { crossOverRate = value; }
        }
        
        public double Resolution
        {
            get { return resolution; }
            set { resolution = value; }
        }

        public double Xmax
        {
            get { return xmax; }
            set { xmax = value; }
        }

        public double Xmin
        {
            get { return xmin; }
            set { xmin = value; }
        }

        public int NbrGenes
        {
            get { return nbrGenes; }
            set { nbrGenes = value; }
        }

        public List<List<int>> Population
        {
            get { return population; }
            set { population = value; }
        }
        

        public int NbrIndv
        {
            get { return nbrIndv; }
            set { nbrIndv = value; }
        }

        public static Int64 NextInt64()
        {
            var bytes = new byte[sizeof(Int64)];
            RNGCryptoServiceProvider Gen = new RNGCryptoServiceProvider();
            Gen.GetBytes(bytes);
            return BitConverter.ToInt64(bytes, 0);
        }

        public AG()
        {
            population = new List<List<int>>();
            initialPopulation = new List<List<int>>();

            randNum = new Random(Guid.NewGuid().GetHashCode());

            this.crossOverRate = 0.6;
            this.mutateRate = 0.01;
            this.numIndvTourney = 10;
            this.nbrGenes = 28;
            this.elitism = false;

            this.initialPoint.X = 3;
            this.initialPoint.Y = 3;
        }


        //nbr genes max 16bits
        //nbrGenes deve ser sempre divisivel por dois pois fica mais facil de partir o cromossomo para separar o x do y 
        public void InitialPopulation(int nbrIndv)
        {
            this.nbrIndv = nbrIndv;
            this.NbrGenes = nbrGenes;
            //this.resolution = Math.Pow(2, nbrGenes);
            
            //6 => initial point
            //192 => 64 moviments
            this.nbrGenes = 192;

            //uint chromossomeResolution = (uint)(Math.Pow(2, (this.nbrGenes/6)) - 1);



            Random randNum1 = new Random(Guid.NewGuid().GetHashCode());
            Random randNum2 = new Random(Guid.NewGuid().GetHashCode());
            Random randNum3 = new Random(Guid.NewGuid().GetHashCode());
            Random randNum4 = new Random(Guid.NewGuid().GetHashCode());
            Random randNum5 = new Random(Guid.NewGuid().GetHashCode());
            Random randNum6 = new Random(Guid.NewGuid().GetHashCode());
            Random randNum7 = new Random(Guid.NewGuid().GetHashCode());
            Random randNum8 = new Random(Guid.NewGuid().GetHashCode());            
            

            while(population.Count() < this.nbrIndv)
            {


                //uint posx = (uint)(randNum1.Next(0,8));
                //uint posy = (uint)(randNum2.Next(0,8));
                //uint part1 = (uint)(randNum3.NextDouble() * chromossomeResolution);
                //uint part2 = (uint)(randNum4.NextDouble() * chromossomeResolution);
                //uint part3 = (uint)(randNum5.NextDouble() * chromossomeResolution);
                //uint part4 = (uint)(randNum6.NextDouble() * chromossomeResolution);
                //uint part5 = (uint)(randNum7.NextDouble() * chromossomeResolution);
                //uint part6 = (uint)(randNum8.NextDouble() * chromossomeResolution);
                uint part1 = (uint)(NextInt64() & 0xFFFFFFFF);
                uint part2 = (uint)(NextInt64() & 0xFFFFFFFF);
                uint part3 = (uint)(NextInt64() & 0xFFFFFFFF);
                uint part4 = (uint)(NextInt64() & 0xFFFFFFFF);
                uint part5 = (uint)(NextInt64() & 0xFFFFFFFF);
                uint part6 = (uint)(NextInt64() & 0x1FFFFFFF);

                                          
                int padLeft = sizeof(int)*8;
                //O padleft e multiplicado por doi pois agora um cromossomo representa o x e o y se a operacao anterior ou (|) resultou em 11 por exemplo
                //o pad left fara ser armazenado na lista 0011 pois pode ter sido o resultda da operação 00 com 11 quando o nbrGenes for 2 por ex.
                //List<int> posX = Convert.ToString(posx, 2).PadLeft(3, '0').ToArray().ToList().ConvertAll<Int32>(delegate(char d) { return Convert.ToInt32(d) - 0x30; });
                //List<int> posY = Convert.ToString(posy, 2).PadLeft(3, '0').ToArray().ToList().ConvertAll<Int32>(delegate(char d) { return Convert.ToInt32(d) - 0x30; });
                List<int> indV1 = Convert.ToString(part1, 2).PadLeft(padLeft, '0').ToArray().ToList().ConvertAll<Int32>(delegate(char d) { return Convert.ToInt32(d) - 0x30; });
                List<int> indV2 = Convert.ToString(part2, 2).PadLeft(padLeft, '0').ToArray().ToList().ConvertAll<Int32>(delegate(char d) { return Convert.ToInt32(d) - 0x30; });
                List<int> indV3 = Convert.ToString(part3, 2).PadLeft(padLeft, '0').ToArray().ToList().ConvertAll<Int32>(delegate(char d) { return Convert.ToInt32(d) - 0x30; });
                List<int> indV4 = Convert.ToString(part4, 2).PadLeft(padLeft, '0').ToArray().ToList().ConvertAll<Int32>(delegate(char d) { return Convert.ToInt32(d) - 0x30; });
                List<int> indV5 = Convert.ToString(part5, 2).PadLeft(padLeft, '0').ToArray().ToList().ConvertAll<Int32>(delegate(char d) { return Convert.ToInt32(d) - 0x30; });
                List<int> indV6 = Convert.ToString(part6, 2).PadLeft(padLeft, '0').ToArray().ToList().ConvertAll<Int32>(delegate(char d) { return Convert.ToInt32(d) - 0x30; });

                List<int> indV = new List<int>();
                //Chromossome codification
                //Initial position
                //indV.AddRange(posX);
                //indV.AddRange(posY);
                //Moviments on board that have eigth possibilities
                indV.AddRange(indV1);
                indV.AddRange(indV2);
                indV.AddRange(indV3);
                indV.AddRange(indV4);
                indV.AddRange(indV5);
                indV.AddRange(indV6);
                                                               
                population.Add(indV);

                printPositions(indV);
            }

            initialPopulation = population;
        }

        List<int> fitnessProportionateSelection()
        {
            
            List<double> fi = new List<double>();
            List<double> pi = new List<double>();
            List<double> ci = new List<double>();

            double S = 0;
            double c = 0;

            //Calculate sum
            foreach (List<int> indV in population)
            {
                double f = calculateFitness(indV);
                S += f;
                fi.Add(f);
            }

            foreach (double f in fi)
            {
                pi.Add(f / S);
            }

            
            for (int i = 0; i < population.Count(); i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    c += pi[j];
                }
                ci.Add(c);

                c = 0;
            }
            
            return rouletteWheel(ci);
        }

        List<int> tourneySelection()
        {
            Random randNum = new Random(Guid.NewGuid().GetHashCode());
            List<List<int>> tourneyIndividuals = new List<List<int>>();

            for (int i = 0; i < this.numIndvTourney; i++)
            {
                int individualIdx = randNum.Next(0, this.nbrIndv-1);

                List<int> individual = new List<int>(population[individualIdx]);

                tourneyIndividuals.Add(individual);
            }

            return getBestIndividual(tourneyIndividuals);          

        }

        List<int> rouletteWheel(List<double> ci)
        {
            double r = randNum.NextDouble();
            int idx = 0;

            for (int i = 0; i < ci.Count() - 1; i++)
            {
                if (ci[i] < r && r <= ci[i + 1])
                {
                    //Console.WriteLine("ci({0}) = {1}",i, ci[i]);
                    //Console.WriteLine("ci({0}) = {1}",i+1, ci[i+1]);
                    idx = i + 1;
                    break;
                }
            }

            return population[idx];
        }

        Point convetToMoviment(int binMoviment)
        {
            Point point = new Point(0,0);

            switch (binMoviment)
            {
                case 0:
                    point.X = +1; point.Y = +2;
                    break;
                case 1:
                    point.X = +2; point.Y = +1;
                    break;
                case 2:
                    point.X = +2; point.Y = -1;
                    break;
                case 3:
                    point.X = +1; point.Y = -2;
                    break;
                case 4:
                    point.X = -1; point.Y = -2;
                    break;
                case 5:
                    point.X = -2; point.Y = -1;
                    break;
                case 6:
                    point.X = -2; point.Y = +1;
                    break;
                case 7:
                    point.X = -1; point.Y = +2;
                    break;                   
            }
            return point;
        }

        List<Point> getCoordinates(List<int> individual)
        {
            List<Point> points = new List<Point>();

            //i = 0 porque chutando uma posiçao inicial depois de 64 movimentos sem repetiçao o 
            //cavalo acaba voltando pra ela depois que o algoritmo achar o melhor caminho
            for (int i = 0; i <= (this.nbrGenes) - 3; i += 3)
            {
                int binMoviment = (individual[i]*4 + individual[i + 1]*2 + individual[i + 2]);
                Point point = convetToMoviment(binMoviment);
                points.Add(point);
            }

            return points;
        }

        Point getInitialPosition(List<int> individual)
        {
            Point point = new Point(0, 0);

            int x = individual[0]*4 + individual[1]*2 + individual[2];
            int y = individual[3]*4 + individual[4]*2 + individual[5];

            point.X = x;
            point.Y = y;

            return point;
        }

        bool checkKnightPath(List<Point> knightPath, Point initPoint)
        {
            bool bres = true;

            List<Point> path = new List<Point>(knightPath);
            path.Add(initPoint);
            for (int i = 0; i < path.Count; i++)
            {
                Point ps = new Point(path[i].X, path[i].Y);

                path.RemoveAt(i);

                bool exists = path.Exists(item => item == ps);
                if (exists == true)
                {
                    bres = false;
                    break;
                }
            }
            

            return bres;
        }

        bool checkKnightPath(List<Point> knightPath)
        {
            bool bres = true;

            var q = from x in knightPath
                    group x by x into g
                    let count = g.Count()
                    orderby count descending
                    select new { Value = g.Key, Count = count };

            int countRepeated = 0;
            foreach (var x in q)
            {
                //Console.WriteLine("Value: " + x.Value + " Count: " + x.Count);
                if (x.Count > 1)
                {
                    countRepeated += x.Count - 1;
                }
            }

            if (countRepeated > 0)
            {
                bres = false;
            }

            return bres;
        }

        bool coordinateIsValid(List<Point> path, Point coordinate)
        {
            bool bres = false;

            if (coordinate.X <= 7 && coordinate.Y <= 7 && coordinate.X >= 0 && coordinate.Y >= 0 && 
                (coordinateAlreadyExist(path, coordinate) == false))
            {
                bres = true;
            }

            return bres;
        }

        bool coordinateIsValid(List<Point> path, int x, int y)
        {
            bool bres = false;
            Point p = new Point(x,y);

            bres = coordinateIsValid(path, p);

            return bres;
        }

        bool coordinateAlreadyExist(List<Point> path, Point coordinate)
        {
            bool bres = false;

            foreach (Point p in path)
            {
                if (p.X == coordinate.X &&
                    p.Y == coordinate.Y)
                {
                    bres = true;
                    break;
                }
            }

            return bres;
        }

        int naviateKnigthOnChessBoard(Point[] steps)
        {
            int value = 0;

            int chessCaseSucessMoved = 0;

            
            List<Point> knigthPath = new List<Point>();
            //knigthPath.Add(initialPoint);

            //The board chess have x and y coodinates x [0..7] and y[0..7]
            Point p = new Point(this.initialPoint.X, this.initialPoint.Y);

            //64 movimentos
            for (int i = 0; i < 64; i++)
            {
                p.X += steps[i].X;
                p.Y += steps[i].Y;

                
                if (p.X <= 7 && p.Y <= 7 && p.X >= 0 && p.Y >= 0)
                {
                    if (coordinateAlreadyExist(knigthPath, p) == false)
                    {
                        chessCaseSucessMoved++;
                        knigthPath.Add(p);
                    }
                    else
                    {
                        break;
                    }

                }
                else
                {
                    break;
                }
                
            }

            
            //List<int> list = new List<int>(new int[] { 19, 23, 29 });

            ////http://www.dotnetperls.com/list-exists
            //for (int i = 0; i < knigthPath.Count; i++)
            //{
            //    Point ps = new Point(knigthPath[i].X, knigthPath[i].Y);

            //    knigthPath.RemoveAt(i);

            //    bool exists = knigthPath.Exists(item => item == ps);
            //    if (exists == true)
            //    {
            //        samePlaces++;
            //    }
            //}

            /*var q = from x in knigthPath
                    group x by x into g
                    let count = g.Count()
                    orderby count descending
                    select new { Value = g.Key, Count = count };

            int countWithoutRepeatition = 0;
            foreach (var x in q)
            {
                //Console.WriteLine("Value: " + x.Value + " Count: " + x.Count);
                if (x.Count == 1)
                {
                    countWithoutRepeatition += x.Count;
                }
            }*/

            value = chessCaseSucessMoved;// +countWithoutRepeatition;// -countRepeated;
            
            return value;
        }

        bool checkValidMoviment(int x, int y, List<Point> path)
        {
            bool bRes = false;
            if (x <= 7 && y <= 7 && x >= 0 && y >= 0 && coordinateAlreadyExist(path, new Point(x,y)) == false)
            {
                bRes = true;
            }

            return bRes;
        }

        bool checkValidMoviment(Point p, List<Point> path)
        {
            bool bRes = false;
            if (p.X <= 7 && p.Y <= 7 && p.X >= 0 && p.Y >= 0 && coordinateAlreadyExist(path, p) == false)
            {
                bRes = true;
            }

            return bRes;
        }

        int calculateNumberOfLegalMoviments(Point p, List<Point> path)
        {
            int nextLegalMovies = 0;
            int x = 0;
            int y = 0;

            if (checkValidMoviment(p.X + 1, p.Y + 2, path) == true)
            {
                nextLegalMovies++;
            }
            if (checkValidMoviment(p.X + 2, p.Y + 1, path) == true)
            {
                nextLegalMovies++;
            }
            if (checkValidMoviment(p.X + 2, p.Y - 1, path) == true)
            {
                nextLegalMovies++;
            }
            if (checkValidMoviment(p.X + 1, p.Y - 2, path) == true)
            {
                nextLegalMovies++;
            }
            if (checkValidMoviment(p.X - 1, p.Y - 2, path) == true)
            {
                nextLegalMovies++;
            }
            if (checkValidMoviment(p.X - 2, p.Y - 1, path) == true)
            {
                nextLegalMovies++;
            }
            if (checkValidMoviment(p.X - 2, p.Y + 1, path) == true)
            {
                nextLegalMovies++;
            }
            if (checkValidMoviment(p.X - 1, p.Y + 2, path) == true)
            {
                nextLegalMovies++;
            }

            return nextLegalMovies;
        }

        int findNextLegalMoveWithLowestNumberOfLegalMovies(List<int> individual)
        {
            int value = 0;

            int chessCaseSucessMoved = 0;

            List<Point> steps = new List<Point>(getCoordinates(individual));

            List<Point> knigthPath = new List<Point>();
            //knigthPath.Add(initialPoint);

            //The board chess have x and y coodinates x [0..7] and y[0..7]
            Point p = new Point(this.initialPoint.X, this.initialPoint.Y);
            int moveBin = Int32.MaxValue;

            //64 movimentos
            for (int i = 0; i < 64; i++)
            {
                p.X += steps[i].X;
                p.Y += steps[i].Y;


                if (p.X <= 7 && p.Y <= 7 && p.X >= 0 && p.Y >= 0 && (coordinateAlreadyExist(knigthPath, p) == false))
                {
                    
                    chessCaseSucessMoved++;
                    knigthPath.Add(p);                    

                }
                else
                {
                    int availableMoves = Int32.MaxValue, newAvailableMoves = 0;
                    
                    for(int j = 0; j < 7; j++)
                    {
                        Point move = convetToMoviment(j);
                        Point newSquare = new Point(p.X + move.X, p.Y + move.Y);

                        if (coordinateIsValid(knigthPath, newSquare) == true)
                        {
                            newAvailableMoves = calculateNumberOfLegalMoviments(newSquare, knigthPath);
                            if (newAvailableMoves < availableMoves)
                            {
                                moveBin = j;
                            }
                        }
                    }
                    
                    break;
                }

            }

            value = moveBin;// +countWithoutRepeatition;// -countRepeated;

            return value;
        }

        int findNextLegalMoveWithLowestNumberOfLegalMoviesAndModifie(List<int> individual)
        {
            int value = 0;

            int chessCaseSucessMoved = 0;

            List<Point> steps = new List<Point>(getCoordinates(individual));

            List<Point> knigthPath = new List<Point>();
            //knigthPath.Add(initialPoint);

            //The board chess have x and y coodinates x [0..7] and y[0..7]
            Point p = new Point(this.initialPoint.X, this.initialPoint.Y);
            int moveBin = -1;

            //64 movimentos
            for (int i = 0; i < 64; i++)
            {
                moveBin = -1;

                //Encontra nova posição para o cavalo atraves do tipo de movimento armazenado em steps
                p.X += steps[i].X;
                p.Y += steps[i].Y;

                //Verifica se a nova posição é valida
                if (p.X <= 7 && p.Y <= 7 && p.X >= 0 && p.Y >= 0 && (coordinateAlreadyExist(knigthPath, p) == false))
                {
                    //Caso seja valida incrementa o numero de casas percoridas com sucesso
                    chessCaseSucessMoved++;
                    //Adiciona a casa na lista de casas percorridas
                    knigthPath.Add(p);

                }
                else
                {
                    //Caso a casa seja invalida decrementa o passo percorrido 
                    //para p.X e p.Y voltar para casa anterior
                    p.X -= steps[i].X;
                    p.Y -= steps[i].Y;

                    //Verificar o problema e tentar corrigir até não dar mais
                    int availableMoves = 8, newAvailableMoves = 0;
                    Point move = new Point();

                    //For para as 8 possibilidades de movimento
                    //Naquela casa que eu voltei, qual movimentação para uma nova casa é possível?
                    //Onde a casa que o cavalo irá possua o menor número de possibilidaddes
                    for (int j = 0; j < 7; j++)
                    {
                        //Obtem o movimento a partir de j
                        move = convetToMoviment(j);

                        //Adiciona esta movimentação no ponto p.X e p.Y para o cavalo ir para a proxima casa
                        Point newSquare = new Point(p.X + move.X, p.Y + move.Y);
                        //Verifica se a proxima casa é valida, ou seja, se não é repetida ou se não está fora do tabuleiro
                        if (coordinateIsValid(knigthPath, newSquare) == true)
                        {
                            //Caso seja valida verifica qual é a proxima casa valida com o menor numero de movimentos
                            newAvailableMoves = calculateNumberOfLegalMoviments(newSquare, knigthPath);
                            if (newAvailableMoves < availableMoves)
                            {
                                //moveBin obtem o próximo movimento válido para a nova casa que terá o menor número de movimentos
                                //vvalidos
                                moveBin = j;
                            }
                            //break;
                        }
                    }

                    //Se moveBin for diferente de -1 quer dizer que foi encontrado uma proxima casa válida
                    //então basta mudar os bits no cromossomo do individuo e atualizar a lista de movimentos stpes
                    if (moveBin != -1)
                    {                                              
                        //Obtem o ponto do cromossomo a partir do qual nao foi possivel mais mexer o cavalo
                        int iniPos = ((chessCaseSucessMoved) * 3);

                        //Faz a alteração no cromossomo do individuo
                        individual[iniPos + 0] = (moveBin & (1 << 2)) == 0 ? 0 : 1;
                        individual[iniPos + 1] = (moveBin & (1 << 1)) == 0 ? 0 : 1;
                        individual[iniPos + 2] = (moveBin & (1 << 0)) == 0 ? 0 : 1;

                        //Atualiza a lista de movimentos
                        steps = new List<Point>(getCoordinates(individual));

                        //Joga o cavalo para nova casa para continuar o laço
                        p.X += steps[i].X;
                        p.Y += steps[i].Y;

                        //Verifica se o proximo movimento realmente é valido
                        if (p.X <= 7 && p.Y <= 7 && p.X >= 0 && p.Y >= 0 && (coordinateAlreadyExist(knigthPath, p) == false))
                        {
                            //Incrementa o numero de casas percoridas e armazena as casas validas percorridas na lista de caminhos
                            //do cavalo
                            chessCaseSucessMoved++;
                            knigthPath.Add(p);

                            //to just one modification on crome
                            //break;
                        }
                        else
                        {
                            //caso o movimento não seja valido para o laço
                            break;
                        }
                    }
                    else
                    {
                        //Caso nenhuma proxima casa seja encontrada para o laço
                        break;
                    }
                }

            }
            
            

            value = moveBin;// +countWithoutRepeatition;// -countRepeated;

            return value;
        }
        public int fitness(List<int> individual)
        {            
            int fitness = 0;

            //Point initialPoint = getInitialPosition(individual);
            List<Point> points = new List<Point>(getCoordinates(individual));
            int value = naviateKnigthOnChessBoard(points.ToArray());

            fitness = value;
            return fitness;

        }

        double calculateFitness(List<int> individual)
        {
            double fitness = 0.0;

            //http://www.c-sharpcorner.com/UploadFile/mgold/GAScheduler04082007210629PM/GAScheduler.aspx
            //fitness = Pl1(individual) + Pl2(individual) + Pl3(individual) + Pl4(individual);

            //fitness = penalizeIndividual(individual);            
            fitness = this.fitness(individual);

            return fitness;
        }

        double function(double x, double y)
        {
            double z = 0.0;

            z = Math.Abs(x * y * Math.Sin((y * Math.PI) / 4));

            return z;
        }

        List<List<int>> onePointCrossOverRoullete()
        {
            Random randNum = new Random(Guid.NewGuid().GetHashCode());
            List<List<int>> parents = new List<List<int>>();

            int position = randNum.Next(0, (this.nbrGenes));

            //Console.WriteLine("Cut in position {0}", position);

            List<int> indv0 = new List<int>(fitnessProportionateSelection());
            List<int> indv1 = new List<int>(fitnessProportionateSelection());

            parents.Add(indv0);
            parents.Add(indv1);

            //showIndividualAndGenome(parents[0]);
            //showIndividualAndGenome(parents[1]);           

            //Console.WriteLine("Position: {0}", position);
            //Console.WriteLine("Count: {0}", parents[0].Count());

            List<int> part0 = parents[0].GetRange(position, (parents[0].Count()) - position);
            List<int> part1 = parents[1].GetRange(position, (parents[1].Count()) - position);

            parents[0].RemoveRange(position, (parents[0].Count()) - position);
            parents[1].RemoveRange(position, (parents[1].Count()) - position);

            /*foreach (int i in parents[0])
            {
                Console.Write(i);

            }
            Console.Write("-");
            foreach (int i in part0)
            {
                Console.Write(i);
            }
            Console.WriteLine();

            foreach (int i in parents[1])
            {
                Console.Write(i);
            }
            Console.Write("-");
            foreach (int i in part1)
            {
                Console.Write(i);
            }
            Console.WriteLine();*/

            parents[0].InsertRange(position, part1);
            parents[1].InsertRange(position, part0);

            //showIndividualAndGenome(parents[0]);    
            //showIndividualAndGenome(parents[1]);

            return parents;
        }

        
        List<List<int>> onePointCrossOverTourney()
        {
            Random randNum = new Random(Guid.NewGuid().GetHashCode());
            List<List<int>> parents = new List<List<int>>();

            //int position = randNum.Next(0, (this.nbrGenes));

            //Console.WriteLine("Cut in position {0}", position);

            List<int> indv0 = new List<int>(tourneySelection());
            //List<int> indv0 = new List<int>(getBestIndividual());
            List<int> indv1 = new List<int>(tourneySelection());


            int iniPos = 0;

            int position = randNum.Next((iniPos*3), (this.nbrGenes));

            parents.Add(indv0);
            parents.Add(indv1);


            List<int> part0 = parents[0].GetRange(position, (parents[0].Count()) - position);
            List<int> part1 = parents[1].GetRange(position, (parents[1].Count()) - position);

            parents[0].RemoveRange(position, (parents[0].Count()) - position);
            parents[1].RemoveRange(position, (parents[1].Count()) - position);
          

            parents[0].InsertRange(position, part1);
            parents[1].InsertRange(position, part0);

            

            return parents;
        }

        public void generateNewPopulationUsingRoullete()
        {
            List<List<int>> newPopulation = new List<List<int>>();

            Random random = new Random(Guid.NewGuid().GetHashCode());

            /*for (int j = 0; j < newPopulation.Count(); j++)
            {
                int fit = fitness(newPopulation[j]);
                int pos = fit * 3;

                for (int i = 0; i < 8; i++)
                {
                    newPopulation[j][pos] = i & (1 << 2);
                    newPopulation[j][pos + 1] = i & (1 << 1);
                    newPopulation[j][pos + 2] = i & (1 << 0);

                    if (fitness(newPopulation[j]) > fit)
                    {
                        break;
                    }
                }
            }*/

            

            while (newPopulation.Count() < (this.nbrIndv))
            {
                if (random.NextDouble() < this.crossOverRate)
                {

                    List<List<int>> newIndividuals = new List<List<int>>(onePointCrossOverRoullete());


                    mutateIndividual(newIndividuals[0]);
                    mutateIndividual(newIndividuals[1]);

                    newPopulation.Add(newIndividuals[0]);

                    newPopulation.Add(newIndividuals[1]);

                  

                }
                else
                {
                    List<int> newIndividual1 = new List<int>(fitnessProportionateSelection());
                    List<int> newIndividual2 = new List<int>(fitnessProportionateSelection());

                    mutateIndividual(newIndividual1);
                    mutateIndividual(newIndividual2);

                    newPopulation.Add(newIndividual1);
                   
                    newPopulation.Add(newIndividual2);
                   
                }

            }

            //while (newPopulation.Count() > this.nbrIndv)
            //{
            //    newPopulation.RemoveAt(getPopulationFitness(newPopulation).ToList().IndexOf(getWorstIndividualFitness(newPopulation)));
            //}

            if (this.heuristic == true)
            {
                for (int i = 0; i < population.Count(); i++)
                {
                    //int move = -1;
                    //do
                    //{
                    findNextLegalMoveWithLowestNumberOfLegalMoviesAndModifie(population[i]);
                    //}while(move != -1);


                }
            }

            
            if (this.elitism == true)
            {
                //List<int> newIndividual1 = new List<int>(getBestIndividual());
                //List<int> newIndividual2 = new List<int>(getSecondBestIndividual());

                List<List<int>> bestInvividuals = new List<List<int>>();

                int nbrBest = numberOfBestIndividuals;
                while (nbrBest > 0)
                {
                    List<int> indv = new List<int>(getBestIndividual(nbrBest));
                    newPopulation.RemoveAt(getPopulationFitness(newPopulation).ToList().IndexOf(getWorstIndividualFitness(newPopulation)));
                    newPopulation.Add(indv);

                    nbrBest--;
                }

            }

            /*foreach (List<int> indv in newPopulation)
            {
                int fit = fitness(indv);
                int pos = fit * 3;

                for (int i = 0; i < 8; i++)
                {
                    indv[pos] = i & (1 << 2);
                    indv[pos+1] = i & (1 << 1);
                    indv[pos+2] = i & (1 << 0);

                    if (fitness(indv) > fit)
                    {
                        break;
                    }
                }
            }*/

           

            population.Clear();
            population = newPopulation;

        }

        public void generateNewPopulationUsingTourney()
        {
            List<List<int>> newPopulation = new List<List<int>>();

            Random random = new Random(Guid.NewGuid().GetHashCode());

            

            //Get the next legal move with with the lowest number of legal mo
            
            //int spaceToElitsm = 0;

            //Caso o elitismo esteja habilitado eu gero uma nova população com menos 2 individuos
            //e posteriormente eu adiciono o melhor individuo da população anterior e um individo eleito pela roleta
            //if (this.elitism == true)
            //    spaceToElitsm = 2;                        
            while (newPopulation.Count() < (this.nbrIndv))
            {
                if (random.NextDouble() < this.crossOverRate)
                {

                    List<List<int>> newIndividuals = new List<List<int>>(onePointCrossOverTourney());


                    mutateIndividual(newIndividuals[0]);
                    mutateIndividual(newIndividuals[1]);

                    newPopulation.Add(newIndividuals[0]);
                      
                    newPopulation.Add(newIndividuals[1]);

                        
                }
                else
                {
                    List<int> newIndividual1 = new List<int>(tourneySelection());
                    List<int> newIndividual2 = new List<int>(tourneySelection());

                    mutateIndividual(newIndividual1);
                    mutateIndividual(newIndividual2);

                    newPopulation.Add(newIndividual1);
                 
                    newPopulation.Add(newIndividual2);
                    
                }

            }


            if (this.heuristic == true)
            {
                for (int i = 0; i < newPopulation.Count(); i++)
                {
                    int move = -1;
                    do
                    {
                        move = findNextLegalMoveWithLowestNumberOfLegalMoviesAndModifie(newPopulation[i]);
                    } while (move != -1);


                }
            }

            //findNextLegalMoveWithLowestNumberOfLegalMoviesAndModifie(getBestIndividual());

            if (this.elitism == true)
            {
                
                //List<int> newIndividual1 = new List<int>(getBestIndividual());
                //List<int> newIndividual2 = new List<int>(getSecondBestIndividual());

                List<List<int>> bestInvividuals = new List<List<int>>();

                int nbrBest = numberOfBestIndividuals;
                while(nbrBest > 0)
                {
                    List<int> indv = new List<int>(getBestIndividual(nbrBest));
                    newPopulation.RemoveAt(getPopulationFitness(newPopulation).ToList().IndexOf(getWorstIndividualFitness(newPopulation)));
                    newPopulation.Add(indv);

                    nbrBest--;
                }

                
                //newPopulation.RemoveAt(getPopulationFitness(newPopulation).ToList().IndexOf(getWorstIndividualFitness(newPopulation)));
                //newPopulation.RemoveAt(getPopulationFitness(newPopulation).ToList().IndexOf(getWorstIndividualFitness(newPopulation)));

                //newPopulation.Add(newIndividual1);
                //newPopulation.Add(newIndividual2);

            }

            population.Clear();
            population = newPopulation;
        }

        

        void mutateIndividual(List<int> individual)
        {
            int posIni = 0;

            Random randNumber = new Random(Guid.NewGuid().GetHashCode());

            /*int posToMutate = randNumber.Next(0, this.nbrGenes-1);

            individual[posToMutate] = (individual[posToMutate] == 0) ? 1 : 0;*/

            for (int i = posIni; i < this.nbrGenes; i++)
            {
                if (randNumber.NextDouble() < this.mutateRate)
                {
                    individual[i] = (individual[i] == 0) ? 1 : 0;
                }
            }
            
        }

        public double getBestIndividualFitness()
        {
            List<double> fitnessIndvList = new List<double>();

            foreach (List<int> individual in this.population)
            {
                double fitness = calculateFitness(individual);
                fitnessIndvList.Add(fitness);
            }

            return fitnessIndvList.Max();
        }

        public List<int> getBestIndividual()
        {
            List<double> fitnessIndvList = new List<double>();

            foreach (List<int> individual in this.population)
            {
                double fitness = calculateFitness(individual);
                fitnessIndvList.Add(fitness);
            }

            return population[fitnessIndvList.IndexOf(fitnessIndvList.Max())];
        }

        public List<int> getSecondBestIndividual()
        {
            List<double> fitnessIndvList = new List<double>();
            
            foreach (List<int> individual in this.population)
            {
                double fitness = calculateFitness(individual);
                fitnessIndvList.Add(fitness);
            }

            
            List<double> fitnessSorted = new List<double>(fitnessIndvList);
            fitnessSorted.Sort();

            return population[fitnessIndvList.IndexOf(fitnessSorted[fitnessSorted.Count() - 2])];
        }

        List<int> getBestIndividual(int pos)
        {
            List<double> fitnessIndvList = new List<double>();

            foreach (List<int> individual in this.population)
            {
                double fitness = calculateFitness(individual);
                fitnessIndvList.Add(fitness);
            }


            List<double> fitnessSorted = new List<double>(fitnessIndvList);
            fitnessSorted.Sort();

            return population[fitnessIndvList.IndexOf(fitnessSorted[fitnessSorted.Count() - pos])];
        }


        public List<int> getBestIndividual(List<List<int>> pop)
        {
            List<double> fitnessIndvList = new List<double>();

            foreach (List<int> individual in pop)
            {
                double fitness = calculateFitness(individual);
                fitnessIndvList.Add(fitness);
            }

            return pop[fitnessIndvList.IndexOf(fitnessIndvList.Max())];
        }

        public double getWorstIndividualFitness()
        {
            List<double> fitnessIndvList = new List<double>();

            foreach (List<int> individual in this.population)
            {
                double fitness = calculateFitness(individual);
                fitnessIndvList.Add(fitness);
            }

            return fitnessIndvList.Min();
        }

        public double getWorstIndividualFitness(List<List<int>> pop)
        {
            List<double> fitnessIndvList = new List<double>();

            foreach (List<int> individual in pop)
            {
                double fitness = calculateFitness(individual);
                fitnessIndvList.Add(fitness);
            }

            return fitnessIndvList.Min();
        }

        public List<int> getWorstIndividual()
        {
            List<double> fitnessIndvList = new List<double>();

            foreach (List<int> individual in this.population)
            {
                double fitness = calculateFitness(individual);
                fitnessIndvList.Add(fitness);
            }

            return population[fitnessIndvList.IndexOf(fitnessIndvList.Min())];
        }

        public double getAverageFitness()
        {
            double Sum = 0;
            foreach (List<int> individual in population)
            {
                Sum += calculateFitness(individual);
            }

            return Sum / this.nbrIndv;
        }

        

        

        public double[] getPopulationFitness()
        {
            List<double> fitness = new List<double>();
            foreach (List<int> individual in this.population)
            {
                fitness.Add(calculateFitness(individual));
            }

            return fitness.ToArray();
        }

        public double[] getPopulationFitness(List<List<int>> pop)
        {
            List<double> fitness = new List<double>();

            foreach (List<int> individual in pop)
            {
                fitness.Add(calculateFitness(individual));
            }

            return fitness.ToArray();
        }

        List<Point> getSquares(Point[] steps)
        {



            List<Point> knigthPath = new List<Point>();
            //knigthPath.Add(initialPoint);

            //The board chess have x and y coodinates x [0..7] and y[0..7]
            Point p = new Point(this.initialPoint.X, this.initialPoint.Y);

            //64 movimentos
            for (int i = 0; i < 64; i++)
            {
                p.X += steps[i].X;
                p.Y += steps[i].Y;


                if (p.X <= 7 && p.Y <= 7 && p.X >= 0 && p.Y >= 0)
                {
                    if (coordinateAlreadyExist(knigthPath, p) == false)
                    {

                        knigthPath.Add(p);
                    }
                    else
                    {
                        break;
                    }

                }
                else
                {
                    break;
                }

            }


            return knigthPath;
        }

        public void printPositions(List<int> individual)
        {
            //Point initialPoint = getInitialPosition(individual);
            List<Point> points = new List<Point>(getCoordinates(individual));
            List<Point> path = getSquares(points.ToArray());

            Console.Write("{0}{1} ", this.initialPoint.X, this.initialPoint.Y);

            foreach (Point p in path)
            {
                Console.Write("{0}{1} ", p.X,p.Y);
            }
            Console.WriteLine();
        }

        public void showPopulation()
        {
            //int idx = 0;
            Console.WriteLine("-----------------   Show actual population   ------------------");
            Console.WriteLine("Total population: {0}", this.nbrIndv);
            Console.WriteLine("Genome resolution: {0} bits", this.nbrGenes);
            Console.WriteLine();

            Console.WriteLine("      -- Individuals --");
            foreach (List<int> indv in this.population)
            {
                Console.Write("Individual: ");

                for (int i = 0; i < indv.Count(); i++)
                {
                    
                    if (((i % 7) == 0) && (i != 0))
                    {
                        Console.Write("-");
                    }
                    Console.Write(indv[i]);
                }
                Console.Write(" - Fitness: {0}", calculateFitness(indv));
                Console.WriteLine();

                //idx++;
            }

            Console.WriteLine();
            Console.Write("Best fitness: {0} - ", getBestIndividualFitness());
            showIndividualAndGenome(getBestIndividual());            
            Console.WriteLine("Worst fitness: {0}\n", getWorstIndividualFitness());
            Console.WriteLine("Average fitness: {0}\n", getAverageFitness());


            //onePointCrossOver();
            /*for (int i = 0; i < 10; i++)
            {
                List<int> indvSelected = fitnessProportionateSelection();
                Console.Write("Individual {0}: ", population.IndexOf(indvSelected));
                foreach (int gene in indvSelected)
                {
                    Console.Write(gene);
                }

                Console.WriteLine();
            }*/


        }

        void showIndividualAndGenome(List<int> individual)
        {
            //Console.Write("Individual {0}: ", population.IndexOf(individual));
            for (int i = 0; i < individual.Count(); i++)
            {

                if (((i % 7) == 0) && (i != 0))
                {
                    Console.Write("-");
                }
                Console.Write(individual[i]);
            }

            Console.WriteLine();
        }

    }
}
